import gulp from "gulp";
import del from "del";
import browserify from "browserify";
import babelify from "babelify";
import reactify from "reactify";
import source from "vinyl-source-stream";
import plumber from 'gulp-plumber';
import webserver from "gulp-webserver";
import jest from "gulp-jest";
import eslint from "gulp-eslint";
import sass from "gulp-sass";
import pleeease from "gulp-pleeease";

gulp.task("default", [
  "build",
  "watch",
  "server"
]);

gulp.task("build", [
  "build:clean",
  "build:react",
  "build:sass",
  "build:html"
])

gulp.task("build:clean", () => {
  del("./build/*")
});

gulp.task("build:react", () => {
  browserify({
    entries: "./src/app.jsx",
    extensions: [".jsx", ".js"],
    debug: true
  })
    .transform(babelify)
    .transform(reactify)
    .bundle()
    .on('error', function (err) {
      console.log(err.message);
      this.emit();
    })
    .pipe(source("bundle.js"))
    .pipe(gulp.dest("./build"))
});

gulp.task("build:sass", () => {
  gulp.src("./src/styles/**/*.scss")
    .pipe(plumber())
    .pipe(sass())
    .pipe(pleeease())
    .pipe(gulp.dest("./build"));
});

gulp.task("build:html", () => {
  gulp.src("./src/html/**/*.html")
    .pipe(gulp.dest("./build"));
});

gulp.task("watch", () => {
  gulp.watch("./src/html/**/*", ["build:html"]);
  gulp.watch("./src/**/*.scss", ["build:sass"]);
  gulp.watch([
    "src/**/*.jsx",
    "src/**/*.js",
    "!./**/__test__/*"
  ], ["build:react"]);
});

gulp.task("lint", () => {
  return gulp.src(["./src/**/*.js", "./src/**/*.jsx"])
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failOnError());
});

gulp.task("test", () => {
  return gulp.src("src/__tests__")
    .pipe(jest({
      rootDir: "src",
      scriptPreprocessor: "../preprocessor.js",
      unmockedModulePathPatterns: [
        "node_modules/react"
      ],
      moduleFileExtensions: ["js"]
    })
    );
});

gulp.task("server", () => {
  gulp.src("./build")
    .pipe(webserver({
      livereload: true,
      fallback: "index.html",
      open: false
    }));
});
