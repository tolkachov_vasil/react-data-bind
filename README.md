# ReactJS + Reflux in ECMAScript 6

To run jest(test), IO.js is required.

- react
- react-router
- reflux
- jest
- bootstrap
- gulp
- browserify
- babelify
- reactify
- eslint
- sass

## Setup
```
npm install -g gulp
npm install
gulp
```

## Test / Lint
```
gulp test
gulp lint
```
