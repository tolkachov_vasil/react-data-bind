import Reflux from 'reflux';

export default Reflux.createActions([
    'update', 'refresh', 'del', 'add'
], { sync: true });
