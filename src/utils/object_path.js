var ObjectByPath = {

  get(obj, path) {
    return Function('o', ' return o.' + path)(obj);
  },

  set(obj, path, value) {
    return Function('o,v', ' o.' + path + '=v')(obj, value);
  },

  add(obj, path, el) {
    return Function('o,e', 'o.' + path + '.push(e)')(obj, el);
  },

  del(obj, path, index) {
    if (index === undefined) return Function('o', 'delete o.' + path)(obj);
    else return Function('o,i', 'o.' + path + '.splice(i,1)')(obj, index);
  }

};

module.exports = ObjectByPath;