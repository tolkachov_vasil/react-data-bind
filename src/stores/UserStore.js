import Reflux from 'reflux';
import UserActions from '../actions/UserActions';
import ObjectHelper from '../utils/object_path';


export default Reflux.createStore({
  listenables: [UserActions],

  init: function () { // init store object structure
    console.log('init');
    this._data = {
      name: 'Ivan',
      rows: [
        { c1: 'aaa', c2: 'aaa' },
        { c1: 'bbb', c2: 'bbb' },
        { c1: 'ccc', c2: 'ccc' }
      ],
      link: 'name',
      last_name: 'Ivanov',
      phone: 55500,
      address: {
        city: 'Minsk',
        street: 'Lenina',
        rooms: [45, 26, { a: 78 }]
      }
    };

    setInterval(() => {
      this._data.phone++;
      this.trigger(this._data);
    }, 3000);
  },

  getInitialState() {
    return this._data;
  },

  update(path, value) {
    ObjectHelper.set(this._data, path, value);
    this.trigger(this._data);
  },

  del(path, index) {
    ObjectHelper.del(this._data, path, index);
    this.trigger(this._data);
  },

  add(path, obj) {
    ObjectHelper.add(this._data, path, obj);
    this.trigger(this._data);
  },

  refresh() {
    this.trigger({}, ...this._data);
  }

});
