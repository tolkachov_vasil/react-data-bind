import React from "react";
import Router, {Route, RouteHandler, DefaultRoute} from "react-router";

import Header from "./components/Header";
import Home from "./components/Home";
import About from "./components/About";

//
// Root component
//
class App extends React.Component {
  render() {
    return (
      <div>
        <Header/>
        <RouteHandler/>
      </div>
    );
  }
}

//
// Routing
//
let routes = (
  <Route name="app" path="/" handler={App}>
    <Route name="home" path="/" handler={Home} />
    <Route name="about" handler={About} />
    <DefaultRoute handler={Home}/>
  </Route>
);

Router.run(routes, Router.HistoryLocation, (Handler) => {
  React.render(<Handler/>, document.body);
});

