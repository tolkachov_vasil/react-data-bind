import React from "react";
import {Nav, Navbar} from "react-bootstrap";
import {NavItemLink} from "react-router-bootstrap";

export default React.createClass({
  render: function(){
    return (
      <header>
        <Navbar brand='React-Bootstrap'>
          <Nav>
            <NavItemLink to="home">Home</NavItemLink>
            <NavItemLink to="about">About</NavItemLink>
          </Nav>
        </Navbar>
      </header>
    );
  }
});
