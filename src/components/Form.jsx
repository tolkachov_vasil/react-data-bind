import React from "react";
import Reflux from "reflux";
import UserStore from '../stores/UserStore';
import UserActions from '../actions/UserActions';
import TextInput from '../components/TextInput';
import DefaultStore from '../utils/default_store';

export default React.createClass({
  mixins: [Reflux.connect(UserStore)],
  render() {
    DefaultStore.store = this.state;
    DefaultStore.update = UserActions.update;
    DefaultStore.del = UserActions.del;
    DefaultStore.add = UserActions.add;

    return (
      <div>
        <TextInput path='link' />
        <br/>
        <TextInput path={this.state.link} />
        <br/>
        {this.state.rows.map((row, i) => {
          return <div key={i} >
            <TextInput path={`rows[${i}].c1`} />
            <a href="#" onClick={() => DefaultStore.del('rows', i) }> ---</a>
          </div>
        }) }
        <br/>
        <a href="#" onClick={() => DefaultStore.add('rows', { c1: 'new' }) }> +++</a>
        <br/>
        <TextInput path='phone' />
        <br/>
        <TextInput path='address.city' />
        <br/>
        <TextInput path='address.street' />
        <br/>
        <TextInput path='address.rooms[2].a' />
        <br/>
      </div>
    );
  }
});
