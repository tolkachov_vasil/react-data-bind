import React from 'react';
import ObjectByPath from '../utils/object_path';
import DefaultStore from '../utils/default_store';

export default React.createClass({

  getInitialState() {
    return { value: '' }
  },

  getDefaultProps() {
    return { path: '[0]' }
  },

  syncValue(val) {
    // console.log(this.props.path, '----sync---', val);
    if (val) {
      DefaultStore.update(this.props.path, val);
      // this.setState({ value: val });
      this.forceUpdate();
    } else {
      return ObjectByPath.get(this.store, this.props.path);
      // this.setState({ value: val });
      // return this.state.value;
    }
  },

  componentWillMount() {
    this.store = DefaultStore.store;
    // this.setState({ value: ObjectByPath.get(this.store, this.props.path) });
  },

  render() {
    // console.log(this.props.path, '------render:');
    return (
      <input type="text"
        value={this.syncValue(null) }
        onChange={e => this.syncValue(e.target.value) }
        />
    );
  }
});
