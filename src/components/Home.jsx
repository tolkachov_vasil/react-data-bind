import React from 'react';
import Form from "./Form";
import UserActions from '../actions/UserActions';

export default React.createClass({
  getInitialState: function () {
    return { off: false };
  },

  componentWillMount() {
  },

  render: function () {

    return (
      <div>
        <input type='checkbox' value={this.state.off} onChange={
          e => {
            this.setState({ off: e.target.checked });
            UserActions.refresh();
          } }/>
        <br/>
        {!this.state.off ?
          <Form /> : null
        }
        <br/>
      </div>
    );
  }
});
